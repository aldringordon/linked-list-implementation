EXEC = tvguide
CC = gcc
OBJ = tvguide.o fileIO.o interface.o linkedList.o comparison.o filter.o
CFLAGS = -Wall -ansi -pedantic -g
DEL = rm -rf

$(EXEC) : $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(EXEC)

tvguide.o : tvguide.c data.h list.h io.h interface.h comparison.h filter.h
	$(CC) $(CFLAGS) tvguide.c -c

fileIO.o : fileIO.c io.h data.h list.h boolean.h
	$(CC) $(CFLAGS) fileIO.c -c

interface.o : interface.c boolean.h interface.h data.h
	$(CC) $(CFLAGS)	interface.c -c

linkedList.o : linkedList.c list.h data.h io.h
	$(CC) $(CFLAGS) linkedList.c -c

comparison.o : comparison.c comparison.h data.h io.h
	$(CC) $(CFLAGS) comparison.c -c

filter.o : filter.c filter.h data.h
	$(CC) $(CFLAGS) filter.c -c

clean :
	$(DEL) $(EXEC) $(OBJ)
