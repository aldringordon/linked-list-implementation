/* Aldrin Gordon 18340587
COMP1000 UNIX AND C PROGRAMMING: SEMESTER 1, 2018 - ASSIGNMENT 

interface.h file

header file for interface.c containing user input / output functions */
void printEntryArray(GuideEntry* entryArray, int size);
void readStrings(char* day, char* method);
