/* Aldrin Gordon 18340587
 * COMP1000 UNIX AND C PROGRAMMINGl SEMESTER 1, 2018 - ASSIGNMENT

io.h file

header file for fileIO.c which contains the functions relating to the 'read' and 'write' functions of a file */
void printEntryFile(char* fileName, GuideEntry* entryArray, int size);
GuideEntry* readInputFile(char* fileName, int* arraySize);
char* getDay(void* data);
char* getName(void* data);
int getHour(void* data);
int getMin(void* data);

#ifndef IO_H
#define IO_H

#define METHOD_LIMIT 5

#endif
