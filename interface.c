/* Aldrin Gordon 18340587
COMP1000 UNIX AND C PROGRAMMING: SEMESTER 1, 2018 - ASSIGNMENT

interface.c file, contains functions relating to user input / output */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "data.h"
#include "boolean.h"
#include "interface.h"

#define UPPERCASE_DIFFERENCE 32

void printEntryArray(GuideEntry* entryArray, int size)
{
	int i;
	for(i = 0; i < size; i++)
	{
		printf("%d:%02d - %s", entryArray[i].time.hour, entryArray[i].time.min, entryArray[i].name);
	}
}

/* Checks if entered string is a valid method 
@param method - method string to validate
	returns valid - FALSE if string is invalid
					TRUE if string is valid */
int checkMethod(char* method)
{
	int valid = FALSE;
	if(strcmp(method, "time") == 0)
	{
		printf("\tSelected: time\n");
		valid = TRUE;
	}
	else if(strcmp(method, "name") == 0)
	{
		printf("\tSelected: name\n");
		valid = TRUE;
	}
	return valid;
}

/* Checks if entered string is a valid day
@param day - day string to validate
	returns valid - FALSE if string is invalid
					TRUE if string is valid*/
int checkDay(char* day)
{
	int valid = FALSE;
	if(strcmp(day, "Monday") == 0)
	{
		printf("\tSelected: Monday\n");
		valid = TRUE;
	}
	else if(strcmp(day, "Tuesday") == 0)
	{
		printf("\tSelected: Tuesday\n");
		valid = TRUE;
	}
	else if(strcmp(day, "Wednesday") == 0)
	{
		printf("\tSelected: Wednesday\n");
		valid = TRUE;
	}
	else if(strcmp(day, "Thursday") == 0)
	{
		printf("\tSelected: Thursday\n");
		valid = TRUE;
	}
	else if(strcmp(day, "Friday") == 0)
	{
		printf("\tSelected: Friday\n");
		valid = TRUE;
	}
	else if(strcmp(day, "Saturday") == 0)
	{
		printf("\tSelected: Saturday\n");
		valid = TRUE;
	}
	else if(strcmp(day, "Sunday") == 0)
	{
		printf("\tSelected: Sunday\n");
		valid = TRUE;
	}
	return valid;
}

/* Formats the method string so that
	- all chars are lowercase

	eg. Input: TiME
		Formatted: time

Based on code from Worksheet 04: Arrays and Strings
void convertString(char* string)
found in functions.c

uppercase char = lowercase char - 32
lowercase char = uppercase char + 32

@param method - method string to be formatted */
void formatMethod(char* method)
{
	int length, i;
	length = strlen(method);
	for(i = 0; i < length; i++)
	{
		if((method[i] > 'A') && (method[i] < 'Z'))
		{
			method[i] = method[i] + UPPERCASE_DIFFERENCE;
		}
	}
}
/* Formats the day string so that
	- the first char is uppercase
	- the remaining chars are lowercase

	eg. Input: monDAy
		Formatted: Monday

Based on code from Worksheet 04: Arrays and Strings
void convertString(char* string)
found in functions.c

uppercase char = lowercase char - 32
lowercase char = uppercase char + 32

@param day - day string to be formatted */
void formatDay(char* day)
{
	int length, i;
	length = strlen(day);
	if((day[0] >= 'a') && (day[0] <= 'z'))
	{
		day[0] = day[0] - UPPERCASE_DIFFERENCE;
	}
	for(i = 1; i < length; i++)
	{
		if((day[i] >= 'A') && (day[i] <= 'Z'))
		{
			day[i] = day[i] + UPPERCASE_DIFFERENCE;
		}
	}
}

/* Reads in day of the week and method from user
Calls format function to format the string
Checks user input is valid
Exports valid strings by reference 
@param char* day - day of the week to be input by user
@param char* method - sorting method to be input by user */
void readStrings(char* day, char* method)
{
	int valid = FALSE;

	while(valid != TRUE)
	{
		printf("Enter day of the week (etc. Monday):\n");
		scanf("%10s", day);
		formatDay(day);
		valid = checkDay(day);
	}

	valid = FALSE;

	while(valid != TRUE)
	{
		printf("Methods:\n    - time\n    - name\nEnter Method:\n");
		scanf("%5s", method);
		formatMethod(method);
		valid = checkMethod(method);
	}
}
