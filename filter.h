/* Aldrin Gordon 18340587
COMP1000 UNIX AND C PROGAMMING: SEMESTER 1, 2018 - ASSIGNMENT

filter.h

header file for filter.c which contains the functions responbible for filtering out GuideEntry array for specific days */
GuideEntry* filterArray(GuideEntry* array, int* size, char* day);
