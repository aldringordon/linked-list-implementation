/* Aldrin Gordon 18340587
COMP1000 UNIX AND C PROGRAMMING: SEMESTER 1, 2018 - ASSIGNMENT

comparison.h

header file for comparison.c which contains the comparison functions to be used by qsort() */
typedef int (*Method)(const void* a, const void* b);
Method methodChoice(char* method);
