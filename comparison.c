/* Aldrin Gordon 18340587
COMP1000 UNIX AND C PROGRAMMING: SEMESTER 1, 2018 - ASSIGNMENT

comparison.c

contains the comparison functions to be used by qsort() to sort the array of GuideEntry's 

man qsort:
       void qsort(void *base, size_t nmemb, size_t size,
                  int (*compar)(const void *, const void *));

base = start of array
nmemb = number of array elements
size = size in bytes of each element in array (sizeof)

int (*fptr)(void* a, void* b) = compares 2 arguments and must return an int 
	int < 0 : a < b
	int == 0 : a == b
	int > 0: a > b

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "data.h"
#include "io.h"
#include "comparison.h"

/* Compares the time of 2 entries by finding the difference in minutes */
int compareTime(const void* a, const void* b)
{
	GuideEntry *x, *y;
	x = (GuideEntry*)a;
	y = (GuideEntry*)b;
	return (((x->time.hour * 60) + x->time.min) - ((y->time.hour * 60) + y->time.min));
}

/* Compares the name of 2 entries by comparing the first char from each name string */
int compareName(const void* a, const void* b)
{
	GuideEntry *x, *y;
	char c1, c2;
	x = (GuideEntry*)a;
	y = (GuideEntry*)b;
	c1 = x->name[0];
	c2 = y->name[0];
	return (c1 - c2);
}

/* Chooses which comparison function to choose from based on the user input stored in method 
function based off Worksheet03: Question 4 - Pointers to functions
code found in order.c line 31*/
Method methodChoice(char* choice)
{
	Method func;
	if(strcmp(choice, "time") == 0)
	{
		func = &compareTime;
	}
	else if(strcmp(choice, "name") == 0)
	{
		func = &compareName;
	}
	else
	{
		func = NULL;
	}
	return func;
}
