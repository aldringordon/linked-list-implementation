/* Aldrin Gordon 18340587
COMP1000 UNIX AND C PROGAMMING: SEMESTER 1, 2018 - ASSIGNMENT

filter.c

conatins functions that handle filtering the entry array */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "data.h"
#include "filter.h"


int getNewArraySize(GuideEntry* array, int size, char* day)
{
    int i, newSize;
    newSize = 0;
    for(i = 0; i < size; i++)
	{
		if(strcmp(array[i].day, day) == 0)
		{
			newSize++;
		}
	}
	return newSize;
}

GuideEntry* filterArray(GuideEntry* array, int* size, char* day)
{
    GuideEntry* newArray;
    int newSize, i, j;
	j = 0;

    newSize = getNewArraySize(array, *size, day);
	newArray = (GuideEntry*)malloc(newSize * sizeof(GuideEntry));

	for(i = 0; i < *size; i++)
	{
		if(strcmp(array[i].day, day) == 0)
		{
			newArray[j] = array[i];
			j++;
		}
	}

	*size = newSize;
	free(array);
    return newArray;
}
