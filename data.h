/* Aldrin Gordon 18340587
COMP1000 UNIX AND C PROGRAMMING: SEMESTER 1, 2018 - ASSIGNMENT

data.h

contains the typedef structures for TV guide entry */
#ifndef DATA_H
#define DATA_H
typedef struct
{
	int hour;
	int min;
} Time;

typedef struct
{
	char name[51];
	char day[11];
	Time time;
} GuideEntry;

#define NAME_LIMIT 50
#define DAY_LIMIT 10

#endif
