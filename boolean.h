/* Aldrin Gordon 18340587
COMP1000 UNIX AND C PROGRAMMING: SEMESTER 1, 2018 - ASSIGNMENT

boolean.h file, contains TRUE & FALSE constants */

#ifndef BOOLEAN_H
#define BOOLEAN_H

#define TRUE 1
#define FALSE 0

#endif
