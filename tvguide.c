/* Aldrin Gordon 18340587
COMP1000 UNIX AND C PROGRAMMING: SEMESTER 1, 2018
ASSIGNMENT
DUE - 08:00 28/05/2018

tvguide.c file, contains main() method

accepts 2 command line parameters:
	- input file name
	- output file name */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "data.h"
#include "list.h"
#include "io.h"
#include "interface.h" /* Contains user input/output functions */
#include "comparison.h"
#include "filter.h"

int main(int argc, char* argv[])
{
	char day[DAY_LIMIT + 1], method[METHOD_LIMIT + 1];
	GuideEntry* entryArray;
	Method choice;
	int arraySize = 0;
	if(argc < 3)
	{
		printf("Error - Usage: ./tvguide [input file] [output file]\n");
	}
	else
	{
		readStrings(day, method); /* in interface.c */
		entryArray = readInputFile(argv[1], &arraySize); /* in fileIO.c */

/* $$$$ TEST CODE $$$
		printf("********* BEFORE QSORT ********\n");
		for(i = 0; i < arraySize; i++)
		{
			printf("***\nINDEX [%d]\nName; %sDay: %s\nTime: %d:%d\n", i, entryArray[i].name, entryArray[i].day, entryArray[i].time.hour, entryArray[i].time.min);
		} */

		choice = methodChoice(method); /* in comparison.c */
		qsort(entryArray, arraySize, sizeof(GuideEntry), choice);

/* $$$$ TEST CODE $$$
        printf("********* AFTER QSORT ********\n");
        for(i = 0; i < arraySize; i++)
        {
            printf("***\nINDEX [%d]\nName; %sDay: %s\nTime: %d:%d\n", i, entryArray[i].name, entryArray[i].day, entryArray[i].time.hour, entryArray[i].time.min);
        } */

		entryArray = filterArray(entryArray, &arraySize, day); /* in filter.c */

/* $$$$ TEST CODE $$$
        printf("********* AFTER FILTER ARRAY ********\n");
        for(i = 0; i < arraySize; i++)
        {
            printf("***\nINDEX [%d]\nName; %sDay: %s\nTime: %d:%d\n", i, entryArray[i].name, entryArray[i].day, entryArray[i].time.hour, entryArray[i].time.min);
        } */

		printEntryFile(argv[2], entryArray, arraySize); /* in fileIO.c */
		printEntryArray(entryArray, arraySize); /* in interface.c */
		
		free(entryArray);
	}
	return 0;
}
