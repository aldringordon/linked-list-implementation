/* Aldrin Gordon 18340587
COMP1000 UNIX AND C PROGRAMMING: SEMESTER 1, 2018 - ASSIGNMENT

fileIO.c file, contains the all functions relating to the 'read' and 'write' functions of a file */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "data.h"
#include "io.h"
#include "list.h"
#include "boolean.h"
void printEntryFile(char* fileName, GuideEntry* entryArray, int size)
{
	FILE* file;
	int i;
	file = fopen(fileName, "w");
	if(file == NULL)
	{
		perror("Error opening output file\n");
	}
	else
	{
		for(i = 0; i < size; i++)
		{
			fprintf(file, "%d:%02d - %s", entryArray[i].time.hour, entryArray[i].time.min, entryArray[i].name);
		}
	}
	fclose(file);
}

/* $$$$$$$$ Prints the contents of the GuideEntry array FOR TESTING PURPOSES $$$$$$$$$ */
void printArray(GuideEntry* array, int length)
{
	int i;
	for(i = 0; i < length; i++)
	{
		printf("***\nINDEX [%d]\nName; %sDay: %s\nTime: %d:%d\n", i, array[i].name, array[i].day, array[i].time.hour, array[i].time.min);
	}
}

/* Getter method to get the name from void* */
char* getDay(void* data)
{
	return ((GuideEntry*)data)->day;
}

/* Getter method to get the name from void* */
char* getName(void* data)
{
	return ((GuideEntry*)data)->name;
}

/* Getter method to get the hour from void* */
int getHour(void* data)
{
	return ((GuideEntry*)data)->time.hour;
}

/* Getter method to get the min from void* */
int getMin(void* data)
{
	return ((GuideEntry*)data)->time.min;
}

/* Copys the contents of the linked list to a dynamically allocated array
typecasts each field in GuideEntry struct into required data type for storage from void*
 * @param list - linked list to copy elements from */
GuideEntry* createEntryArray(LinkedList* list)
{
	GuideEntry* entryArray;
	int i;
	LinkedListNode* current;
	i = 0;
	current = list->head;
	entryArray = (GuideEntry*)malloc(list->size * sizeof(GuideEntry));
	while(current != NULL)
	{
		entryArray[i].time.min = getMin(current->data); 
		entryArray[i].time.hour = getHour(current->data);
		strcpy(entryArray[i].name, getName(current->data));
		strcpy(entryArray[i].day, getDay(current->data));

		i++;
		current = current->next;
	}

/* $$$ TEST $$$
	printf("$$$ ARRAY $$$\n");
	printArray(entryArray, list->size); */

	return entryArray;
}

/* Reads in the TVGuideEntries from input file and stores them in a linked list
@param FILE* file - the input file to be read from */
GuideEntry* storeEntries(FILE* file, int* arraySize)
{
	LinkedList* list;
	GuideEntry* entry, *entryArray;
	int done = FALSE;
	entry = (GuideEntry*)malloc(sizeof(GuideEntry));
	createLinkedList(&list);
	while(done != TRUE)
	{
		if(fgets(entry->name, NAME_LIMIT, file) == NULL)
		{
			done = TRUE;
		}
		else
		{
			fscanf(file, "%10s %d:%d\n", entry->day, &entry->time.hour, &entry->time.min);

/* $$$ TEST $$$
			printf("****** BEFORE INSERT ELEMENT *******\n");
			printf("%s", entry->name);
			printf("%s %d:%d\n", entry->day, entry->time.hour, entry->time.min);
			printf("*********\n"); */

			insertStartElement(&list, entry);

/* $$$ TEST $$$
			printf("****** PRINT SINGLE NODE ******\n");
			printSingleNode(list->head);
			printf("******** END SINGLE NODE ******\n");
			printf("****** PRINT LIST *****\n");
			printList(list);
			printf("***** END LIST *********\n"); */
		}
	}
/* $$$ TEST $$$
	printf("$$$ LINKED_LIST $$$\n");
	printList(list); */

	*arraySize = list->size;
	entryArray = createEntryArray(list);
	free(entry);
	freeList(list);
	return entryArray;
}

/* Opens the input file for reading
@param char* fileName - the name of the input file to open */
GuideEntry* readInputFile(char* fileName, int* arraySize)
{
	FILE* file;
	GuideEntry* entryArray;
	file = fopen(fileName, "r");
	if(file == NULL)
	{
		perror("Error opening input file\n");
		entryArray = NULL;
	}
	else
	{
		entryArray = storeEntries(file, arraySize);
		fclose(file);
	}
	return entryArray;
}
